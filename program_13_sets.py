# There are array of integers. There are also two dis-joint sets A, B each containing m integers.You like all integers in set A and dislike all integers in set B. Your initial happiness is 0,or each i integer in array
#If i belongs to set A your happiness increases by 1
#if I belongs to setB your happiness decreases by 1
#otherwise your happiness doesnt change, output the final happiness
print('input size in format 1 2 depending on size of n')
nm = map(int, input('input n size and m size by space:').split())#input for size of n and m elements
print('enter array of elements separated by spaces')
array = map(int, input('input array of elements').split())# array of elenments
print('enter set of elements by size m separated by spaces')
set_A = set(map(int, input('input elements to set A').split()))#elements for set A
set_B = set(map(int, input('input elements to set B').split()))#elements for set B
happiness = 0 #intialising happiness
for i in array:
    if i in set_A:
        happiness += 1#increments happiness if condition is satisfied
    elif i in set_B:
        happiness -= 1#decrements happiness if condition is satisfied
print(happiness)#final output of happiness