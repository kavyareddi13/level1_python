#program that prints the numbers from 1 to 100 and for multiples of '3' print "Fizz" instead of the number and for the multiples of '5' print "Buzz"
for number in range(1,101):#loops from 1 to 100
    if number % 3 == 0:#if reminder of number when divided by 3 equals to zero
        print('Fizz')
    elif number % 5 == 0:#if reminder of number when divided by 5 equals to zero
        print('Buzz')
    else:
        print(number)
