#Python program to combine two dictionary adding values for common keys.
#Python ships with a module that contains a number of container data types called Collections.
from collections import Counter #Counter allows us to count the occurrences of a particular item
d1 = {'a': 100, 'b': 200, 'c':300}#initialising dictionary 1
d2 = {'a': 300, 'b': 200, 'd':400}#initializing dictionary 2
d = Counter(d1) + Counter(d2)#finds the common elements from the dictionaries and sums up them
print(d) #output
#OR
d1 = {'a':100, 'b':200, 'c':300}
d2 = {'a':300, 'b':200, 'd':400}
for key, value in d1.items():
    if key in d2:
        d1[key]=value +d2[key]#it adds value of key
        del d2[key]#it is used so that it can detect uncommon elements
z = {**d1,**d2} #merges d1 and d2 in to z
print(z)