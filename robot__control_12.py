#A robot moves in a plane starting from the original point (0,0). The robot can move toward UP, DOWN, LEFT and RIGHT with a given steps. The trace of robot movement is shown as the following:
#UP 5
#DOWN 3
#LEFT 3
#RIGHT 2
#The numbers after the direction are steps. Please write a program to compute the distance from current position after a sequence of movement and original point. If the distance is a float, then just print the nearest integer.
import math
position = [0,0]# initial position
print('input as UP 5 DOWN 4....')

while True:
    s = input()# input from user
    if not s:#breaks the condition if input is out of the s
        break

    move = s.split()#splitting direction and steps
    direction = move[0]#intializing index 0 to move
    steps = float(move[1])#intializing index 1 to steps
#conditions for directions
    if direction=="UP":
        position[0]+=steps
    elif direction=="DOWN":
        position[0]-=steps
    elif direction=="LEFT":
        position[1]-=steps
    elif direction=="RIGHT":
        position[1]+=steps
    else:
        print('enter a valid position')

print(math.sqrt(position[1]**2+position[0]**2))#prints the distance between initial and final positions